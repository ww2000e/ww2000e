# 记录手动部署ceph nautilus版本的过程

>主要参考官方文档： https://docs.ceph.com/en/nautilus/install/manual-deployment/


## 虚机环境准备

| 虚机主机名 | 配置                                         |
| ---------- | -------------------------------------------- |
| node1  | eth0: 192.168.1.10      eth1: 192.168.56.10  |
| node2  | eth0: 192.168.1.11      eth1: 192.168.56.10 |
| node3  | eth0: 192.168.1.12      eth1: 192.168.56.10        |



## 替换国内源

使用阿里的ceph源: 

/etc/yum.repos.d/ceph.repo

``` ini
[Ceph]
name=Ceph packages for $basearch
baseurl=https://mirrors.aliyun.com/ceph/rpm-nautilus/el7/$basearch
enabled=1
gpgcheck=1
type=rpm-md
gpgkey=https://mirrors.aliyun.com/ceph/keys/release.asc

[Ceph-noarch]
name=Ceph noarch packages
baseurl=https://mirrors.aliyun.com/ceph/rpm-nautilus/el7/noarch
enabled=1
gpgcheck=1
type=rpm-md
gpgkey=https://mirrors.aliyun.com/ceph/keys/release.asc

[ceph-source]
name=Ceph source packages
baseurl=https://mirrors.aliyun.com/ceph/rpm-nautilus/el7/SRPMS
enabled=1
gpgcheck=1
type=rpm-md
gpgkey=https://mirrors.aliyun.com/ceph/keys/release.asc
```



## 安装ceph

```
# 三个节点执行
$ yum install ceph
```



## 配置ceph

### 生成一个FSID

```
uuidgen
生成一个id
efd6af9b-276e-4fe1-b961-62c43de20313
```



### 配置文件ceph.conf

``` ini
[global]
fsid = efd6af9b-276e-4fe1-b961-62c43de20313

public network = 192.168.1.0/24
cluster network = 192.168.56.0/24

mon initial members = node1, node2, node3
mon host = 192.168.1.10, 192.168.1.11, 192.168.1.12

osd pool default size = 2
osd pool default min size = 2
osd pool default pg num = 128
osd pool default pgp num = 128
```



### ceph mon

* 创建集群密钥环，生成monitor密钥

```
ceph-authtool --create-keyring /tmp/ceph.mon.keyring --gen-key -n mon. --cap mon 'allow *'
```

* 创建admin密钥环，创建client.admin用户并加入密钥环

```
ceph-authtool --create-keyring /etc/ceph/ceph.client.admin.keyring --gen-key -n client.admin --cap mon 'allow *' --cap osd 'allow *' --cap mds 'allow *' --cap mgr 'allow *'
```

* 创建bootstrap-osd密钥环，创建client.bootstrap-osd用户，将用户加入密钥环

```
ceph-authtool --create-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring --gen-key -n client.bootstrap-osd --cap mon 'profile bootstrap-osd' --cap mgr 'allow r'
```

* 将创建的密钥加入ceph.mon.keyring

```
ceph-authtool /tmp/ceph.mon.keyring --import-keyring /etc/ceph/ceph.client.admin.keyring
  
ceph-authtool /tmp/ceph.mon.keyring --import-keyring /var/lib/ceph/bootstrap-osd/ceph.keyring
```

* 修改文件属性

```
chown ceph:ceph /tmp/ceph.mon.keyring
```

* 创建monitor map

```
monmaptool --create --add node1 192.168.1.10 --fsid efd6af9b-276e-4fe1-b961-62c43de20313 /tmp/monmap
```

* 创建monitor默认数据目录

```
# 目录的名字为{cluster-name}-{hostname}
mkdir /var/lib/ceph/mon/ceph-node1
```

* 设置monitor进程的map和密钥环

```
ceph-mon --mkfs -i node1 --monmap /tmp/monmap --keyring /tmp/ceph.mon.keyring
```

* 启动monitor

```
chown -R ceph:ceph /var/lib/ceph
systemctl start ceph-mon@node1
systemctl enable ceph-mon@node1
```
  
* 启用msgr2

```
ceph mon enable-msgr2
```
  
* 检查monitor

```
  #查看mon进程状态
  systemctl status ceph-mon@node1
  
  #查看ceph状态
  ceph -s 
  
  cluster:
  id:     efd6af9b-276e-4fe1-b961-62c43de20313
  health: HEALTH_OK
  
  services:
  mon: 1 daemons, quorum node1 (age 66s)
  mgr: no daemons active
  osd: 0 osds: 0 up, 0 in
  
  data:
  pools:   0 pools, 0 pgs
  objects: 0 objects, 0 B
  usage:   0 B used, 0 B / 0 B avail
  pgs:     
  
  
```

### 增加mon

* 登陆新的要安装mon的主机，以node2为示例

* 创建目录
```
mkdir /var/lib/ceph/mon/ceph-node2
```

* 获取mon的keyring
```
ceph auth get mon. -o /tmp/monkeyring
```

* 获取mon的map
```
ceph mon getmap -o /tmp/monmap
```

* 初始化mon的数据目录
```
ceph-mon -i node2 --mkfs --monmap /tmp/monmap --keyring /tmp/monkeyring
```

* 编辑ceph.conf
```
增加里面值
mon initial members 
mon host 
```

* 启动monitor
```
chown -R ceph:ceph /var/lib/ceph
systemctl start ceph-mon@node2
systemctl enable ceph-mon@node2
```

其他重复即可


### ceph mgr
mymgr为自定义的名字

* 创建认证密钥

```
ceph auth get-or-create mgr.mymgr mon 'allow profile mgr' osd 'allow *' mds 'allow *'
```

* 移动密钥到相应目录中

```
ceph auth get-or-create mgr.mymgr -o mgrkeyring
mkdir -p /var/lib/ceph/mgr/ceph-mymgr/
mv mgrkeyring /var/lib/ceph/mgr/ceph-mymgr/keyring
```

* 启动ceph-mgr进程

```
ceph-mgr -i mymgr
```

* 检查ceph状态

```
ceph -s 

...
mrg active: mymgr
...
```

  

### ceph osd

如果没有bootstrap-osd的keyring，则创建keyring
```
ceph auth get client.bootstrap-osd -o /var/lib/ceph/bootstrap-osd/ceph.keyring
```

#### bluestore
在创建bluestore

* 创建OSD

```
ceph-volume lvm create --data {data-path}
```

* 另一种方法，分两步创建

  1. 准备OSD

```
ceph-volume lvm prepare --data {data-path} {data-path}

ceph-volume lvm list
```
  
  2. 激活OSD
  
```
ceph-volume lvm activate {ID} {FSID}
```
  

#### filestore

* 创建osd

```
ceph-volume lvm create --filestore --data {data-path} --journal {journal-path}
```

  

* 另一种方法，分两步创建

  1. 准备OSD

```
ceph-volume lvm activate --filestore {ID} {FSID}
```

  2. 激活OSD

```
ceph-volume lvm activate --filestore 0 a7f64266-0894-4f1e-a635-d0aeaca0e993
```


#### 手动添加osd

```
UUID=$(uuidgen)

OSD_SECRET=$(ceph-authtool --gen-print-key)

ID=$(echo "{\"cephx_secret\": \"$OSD_SECRET\"}" | \
   ceph osd new $UUID -i - \
   -n client.bootstrap-osd -k /var/lib/ceph/bootstrap-osd/ceph.keyring)

mkdir /var/lib/ceph/osd/ceph-$ID

mkfs.xfs /dev/{DEV}
mount /dev/{DEV} /var/lib/ceph/osd/ceph-$ID

ceph-authtool --create-keyring /var/lib/ceph/osd/ceph-$ID/keyring \
     --name osd.$ID --add-key $OSD_SECRET

ceph-osd -i $ID --mkfs --osd-uuid $UUID

chown -R ceph:ceph /var/lib/ceph/osd/ceph-$ID

systemctl enable ceph-osd@$ID
systemctl start ceph-osd@$ID

```
