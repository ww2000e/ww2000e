ceph学习笔记
# 命令整理
## 系统管理命令

```
# start all daemons
sudo systemctl start ceph.target
  
# check status of osd.12
sudo systemctl status ceph-osd@12
  
# list the Ceph systemd units on a node
sudo systemctl status ceph\*.service ceph\*.target
  
# STARTING ALL DAEMONS BY TYPE
sudo systemctl start ceph-osd.target
sudo systemctl start ceph-mon.target
sudo systemctl start ceph-mds.target
  
# STARTING A DAEMON
sudo systemctl start ceph-osd@{id}
sudo systemctl start ceph-mon@{hostname}
sudo systemctl start ceph-mds@{hostname}
```

## ceph命令

* 查看pool列表
```
ceph osd lspools
```

* 创建pool
```
ceph osd pool create {pool-name} {pg-num}
```

* 删除pool
```
# If you created your own rules for a pool you created, you should consider removing them when you no longer need your pool:
ceph osd pool get {pool-name} crush_rule

ceph osd pool delete {pool-name} [{pool-name} --yes-i-really-really-mean-it]
```

* 重命名pool
```
ceph osd pool rename {current-pool-name} {new-pool-name}
```

* 查看pool状态
```
rados df
ceph osd pool stats [{pool-name}]
```

* pool绑定application
```
# application-name: cephfs, rbd, rgw
ceph osd pool application enable {pool-name} {application-name}
```

* pool创建快照
```
ceph osd pool mksnap {pool-name} {snap-name}
```

* 删除pool快照
```
ceph osd pool rmsnap {pool-name} {snap-name}
```

* 设置对象副本数
```
ceph osd pool set {poolname} size {num-replicas}
```

* 检查集群的数据用量及其在存储池内的分布情况

```
# GLOBAL信息
#   SIZE： 集群的总容量。
#   AVAIL： 集群的可用空间总量。
#   RAW USED：已用存储空间总量。
#   % RAW USED：已用存储空间比率。用此值对比 full ratio 和 near full ratio 来确保不会用尽集群空间。
# POOLS信息
#   SIZE： 集群的总容量。
#   AVAIL： 集群的可用空间总量。
#   RAW USED：已用存储空间总量。
#   % RAW USED：已用存储空间比率。用此值对比 full ratio 和 near full ratio 来确保不会用尽集群空间。
ceph df  
```

* 检查osd状态

```
# Ceph 会打印 CRUSH 树，包括 host 的名称、它上面的 OSD 例程、状态及权重
ceph osd stat  
ceph osd dump
```

* 检查mon状态

```
ceph mon stat
ceph mon dump
# 检查监视器的法定人数状态，执行下面的命令
ceph quorum_status -f json-pretty
```

* 检查mds状态
```
ceph mds stat
ceph mds dump
```

* 查看文件系统
```
ceph fs ls
```

* 查看fs dump
```
ceph fs dump
```
* 查看fs
```
ceph fs get myfs
```

* 设置fs
```
ceph fs set myfs <val> <val>
```

* 设置cephfs最大文件大小
```
ceph fs set <fs name> max_file_size <size in bytes>
```

* 查看pg状态

```
# PG IDs 由存储池号（不是存储池名字）、后面跟一个点（ . ）、再加一个 16 进制数字的 PG ID
{pool-num}.{pg-id}
# 获取 PG 列表
ceph pg dump
# 查看指定 PG 的 Acting Set 或 Up Set 中包含的 OSD
ceph pg map {pg-num}
查看所有 PG 状态
ceph pg stat
# 找出故障pg
ceph pg dump_stuck [unclean|inactive|stale|undersized|degraded]
```
* 显示tunables
```
ceph osd crush show-tunables
```

* ceph config
```
ceph config help <option>
```

* mixed version ceph
```
ceph daemon <name> config help [option]
```

* RUNTIME CHANGES
```
ceph config set osd.123 debug_ms 20
```

* VIEWING RUNTIME SETTINGS
```
// 显示非默认项
ceph config show osd.0
// 显示特殊项
ceph config show osd.0 debug_osd
// 显示所有项
ceph config show-with-defaults osd.0
// 从本机使用admin socket查看所有设置
ceph daemon osd.0 config show
// 显示当前设置
ceph daemon osd.0 config diff
// 显示非默认设置
ceph daemon osd.0 config get debug_osd
```

* 临时改变设置
```
0ceph tell <name> config set <option> <value>
ceph tell osd.123 config set debug_osd 20
ceph tell osd.* config set debug_osd 20
```
```
ceph daemon <name> config set <option> <value>
ceph daemon osd.4 config set debug_osd 20
```

* 查看对象副本数

```
ceph osd dump | grep 'replicated size'
```

* AUTOSCALING PG

```
# off # on # warn
ceph osd pool set <pool-name> pg_autoscale_mode <mode>

ceph config set global osd_pool_default_pg_autoscale_mode <mode>
```

* 查看 PG SCALING RECOMMENDATIONS

```
ceph osd pool autoscale-status
```

* AUTOMATED SCALING

```
ceph config set global mon_target_pg_per_osd 100
```

* 查看crush

```
ceph osd crush tree
```

* 列出规则
```
ceph osd crush rule ls
```

* 存储crush规则
```
ceph osd crush rule dump
```

* 编辑crush map

```
ceph osd getcrushmap -o {compiled-crushmap-filename}

crushtool -d {compiled-crushmap-filename} -o {decompiled-crushmap-filename}

crushtool -i original --reclassify \
    --set-subtree-class default hdd \
    --reclassify-root default hdd \
    --reclassify-bucket %-ssd ssd default \
    --reclassify-bucket ssd ssd default \
    -o adjusted
    
crushtool -i original --compare adjusted

ceph osd setcrushmap -i adjusted
```

* 更改tunable
```
ceph osd crush tunables {PROFILE}
```
- 开启v2协议

```
ceph mon enable-msgr2  
```
* 创建缓存层

* 删除缓存层 

## rbd
* 查看ceph默认资源池rbd里面的镜像
```
rbd ls
```

* 创建1024MB的镜像
```
rbd create rbd1 --size 1024
```
--image-feature 指定特性

* 查看xxx.img的具体的具体信息
```
rbd info xxx.img  
```

* 删除xxx.img
```
rbd rm xxx.img  
```

* 拷贝镜像aaa.img为bbb.img
```
rbd cp aaa.img bbb.img  
```

* 重命名aaa.img为bbb.img
```
rbd rename aaa.img  bbb.img  
```

* 将本地的aaa.img导入ceph集群中
```
rbd import aaa.img  
```

* 将ceph集群中aaa.img导出到本地
```
rbd export aaa.img
```

* 导入原有虚机镜像, format为2，--order 25 表示object size为32M
```
rbd import --image-format 2 win2008_32.img rbd/win2008_test.img　--order 25
```
（将win2008_32.img导入rbd资源池，并命名为win2008_test.img，因为在快照保护时format必须是2，import默认format是1）

* 创建镜像快照（给win2008_test.img创建名为win2008Snap 的快照）
```
rbd snap create --snap win2008Snap rbd/win2008_test.img
```

* 保护快照
```
rbd snap protect rbd/win2008_test.img@win2008Snap
```
(保护win2008_test的快照文件) 


* 克隆快照(克隆rbd资源池中的win2008_test的快照 至rbd/newstest.img中)
```
rbd clone rbd/win2008_test.img@win2008Snap rbd/newtest.img
```

* 快照去保护  
```
rbd snap unprotect rbd/win2008_test.img@win2008Snap
```

* 查看镜像是否有快照 
```
rbd snap ls rbd/test3_temp1_clone.img
```

* 删除快照
```
rbd snap rm rbd/win2008_test.img@win2008Snap
```

* 还原快照克隆出来的镜像   
```
rbd flatten rbd/test.img
```

* 查看子镜像 
```
rbd children 资源池名/镜像名@快照名 
rbd children rbd/test.img@test_snp.img
```

* 将块设备映射到本地 
```
rbd map disk01 –pool rbd –id admin
```

* 查看所有的映射
```
rbd showmapped
```
ceph jewel后默认用镜像格式为2,包含以下特性:
layering: layering support
exclusive-lock: exclusive locking support
object-map: object map support (requires exclusive-lock)
deep-flatten: snapshot flatten support
fast-diff: fast diff calculations (requires object-map)

* 取消映射
```
rbd unmap /dev/rbd1
```

* 关闭镜像feature
```
rbd feature disable xxxx exclusive-lock object-map deep-flatten fast-diff
```

* rbd resize rbd/test.img -s 10G
# ceph说明
## MONITORS
* 最少3个节点，实验环境可以1个
* v2协议监听3300，v1协议监听6789
* 默认情况下使用以下路径
  ```
  /var/lib/ceph/mon/$cluster-$id
  ```

## OSDS

* 默认情况下osd服务的数据存放在这里

  ```
  /var/lib/ceph/osd/$cluster-$id
  ```

## 网络

* 默认情况下服务一般使用6800:7300端口
* mon只使用public network
* mds和mgr监听6800开始端口
* osd使用6800开始的端口
* osd使用4个端口,分别用于clients/monitors，other osds，心跳占用2个
* osd心跳，对象复制，数据恢复都会使用cluster network
* cluster network最好是单独的内网网段

## MESSENGER V2

- A *secure* mode that encrypts all data passing over the network

- Improved encapsulation of authentication payloads, enabling future integration of new authentication modes like Kerberos

- Improved earlier feature advertisement and negotiation, enabling



## cephx配置

https://docs.ceph.com/docs/nautilus/rados/configuration/auth-config-ref/

## mon配置

* mon管理着cluster map，包含所有mon，osd服务，mds的位置



## 健康检查

### MONITOR

* MON_DOWN
* MON_CLOCK_SKEW
* MON_MSGR2_NOT_ENABLED

### MANAGER

* MGR_MODULE_DEPENDENCY
* MGR_MODULE_ERROR

### OSDS

* OSD_DOWN

* OSD_<CRUSH TYPE>_DOWN

* OSD_ORPHAN

  ```
  ceph osd crush rm osd.<id>
  ```

* OSD_OUT_OF_ORDER_FULL

  ```
  ceph osd set-backfillfull-ratio <ratio>
  ceph osd set-nearfull-ratio <ratio>
  ceph osd set-full-ratio <ratio>
  ```

* OSD_FULL

  ```
  ceph df
  ceph osd dump | grep full_ratio
  ceph osd set-full-ratio <ratio>
  ```

* OSD_BACKFILLFULL

* OSD_NEARFULL

* OSDMAP_FLAGS

  ```
  full - the cluster is flagged as full and cannot serve writes
  
  pauserd, pausewr - paused reads or writes
  
  noup - OSDs are not allowed to start.集群不会将任何down osd标记为up状态。
  
  nodown - OSD failure reports are being ignored, such that the monitors will not mark OSDs down.集群不会将任何osd标记为down
  
  noin - OSDs that were previously marked out will not be marked back in when they start
  
  noout - down OSDs will not automatically be marked out after the configured interval.集群不会将任何OSD标记为out，无论其实际状态如何。这将会把所有的OSD保留在ceph集群中
  
  nobackfill, norecover, norebalance - recovery or data rebalancing is suspended.不做集群数据回填，不做集群恢复，不做集群再平衡
  
  noscrub, nodeep_scrub - scrubbing is disabled.集群不做osd清理，集群不做osd深度清理
  
  notieragent - cache tiering activity is suspended.禁用缓存分层代理
  ```

* OSD_FLAGS

  ```
  noup: these OSDs are not allowed to start.集群不会将任何down osd标记为up状态。
  
  nodown: failure reports for these OSDs will be ignored.集群不会将任何osd标记为down
  
  noin: if these OSDs were previously marked out automatically after a failure, they will not be marked in when they start.集群不允许任何新的osd加入集群
  
  noout: if these OSDs are down they will not automatically be marked out after the configured interval
  
  ################################
  ceph osd set-group <flags> <who>
  ceph osd unset-group <flags> <who>
  for example: 
  ceph osd set-group noup,noout osd.0 osd.1
  ceph osd unset-group noup,noout osd.0 osd.1
  ceph osd set-group noup,noout host-foo
  ceph osd unset-group noup,noout host-foo
  ceph osd set-group noup,noout class-hdd
  ceph osd unset-group noup,noout class-hdd
  ```

* OLD_CRUSH_TUNABLES

* OLD_CRUSH_STRAW_CALC_VERSION

* CACHE_POOL_NO_HIT_SET

  ```
  ceph osd pool set <poolname> hit_set_type <type>
  ceph osd pool set <poolname> hit_set_period <period-in-seconds>
  ceph osd pool set <poolname> hit_set_count <number-of-hitsets>
  ceph osd pool set <poolname> hit_set_fpp <target-false-positive-rate>
  ```

  

* OSD_NO_SORTBITWISE

  ```
  ceph osd set sortbitwise
  ```

  

* POOL_FULL

  ```
  ceph df detail
  #You can either raise the pool quota with
  ceph osd pool set-quota <poolname> max_objects <num-objects>
  ceph osd pool set-quota <poolname> max_bytes <num-bytes>
  #or delete some existing data to reduce utilization.
  ```

  

* BLUEFS_SPILLOVER

  ```
  # This warning can be disabled on all OSDs with:
  ceph config set osd bluestore_warn_on_bluefs_spillover false
  # Alternatively, it can be disabled on a specific OSD with:
  ceph config set osd.123 bluestore_warn_on_bluefs_spillover false
  
  ceph-bluestore-tool bluefs-bdev-expand --path /var/lib/ceph/osd/ceph-$ID
  ```

  

* BLUEFS_AVAILABLE_SPACE

  ```
  # To check how much space is free for BlueFS do:
  ceph daemon osd.123 bluestore bluefs available
  ```

  

* BLUEFS_LOW_SPACE

  ```
  ceph daemon osd.123 bluestore bluefs available <alloc-unit-size>
  ```

  

* BLUESTORE_FRAGMENTATION

  ```
  #To inspect BlueStore fragmentation one can do:
  ceph daemon osd.123 bluestore allocator score block
  # If detailed report of free fragments is required do:
  ceph daemon osd.123 bluestore allocator dump block
  # In case when handling OSD process that is not running fragmentation can be inspected with ceph-bluestore-tool. Get fragmentation score:
  ceph-bluestore-tool --path /var/lib/ceph/osd/ceph-123 --allocator block free-score
  # And dump detailed free chunks:
  ceph-bluestore-tool --path /var/lib/ceph/osd/ceph-123 --allocator block free-dump
  ```

  

* BLUESTORE_LEGACY_STATFS

  ```
  systemctl stop ceph-osd@123
  ceph-bluestore-tool repair --path /var/lib/ceph/osd/ceph-123
  systemctl start ceph-osd@123
  
  ceph config set global bluestore_warn_on_legacy_statfs false
  ```

  

* BLUESTORE_DISK_SIZE_MISMATCH

  ```
  ceph osd out osd.$N
  while ! ceph osd safe-to-destroy osd.$N ; do sleep 1m ; done
  ceph osd destroy osd.$N
  ceph-volume lvm zap /path/to/device
  ceph-volume lvm create --osd-id $N --data /path/to/device
  ```

  

### DEVICE HEALTH

* DEVICE_HEALTH

  ```
  ceph device info <device-id>
  ceph device set-life-expectancy <device-id> <from> <to>
  ```

* DEVICE_HEALTH_IN_USE

* DEVICE_HEALTH_TOOMANY

### DATA HEALTH (POOLS & PLACEMENT GROUPS)

* PG_AVAILABILITY

  ```
  ceph health detail
  ceph tell <pgid> query
  ```

* PG_DEGRADED

  ```
  ceph health detail
  ceph tell <pgid> query
  ```

  

* PG_RECOVERY_FULL

* PG_BACKFILL_FULL

* PG_DAMAGED

* OSD_SCRUB_ERRORS

* LARGE_OMAP_OBJECTS

  ```
  ceph config set osd osd_deep_scrub_large_omap_object_key_threshold <keys>
  ceph config set osd osd_deep_scrub_large_omap_object_value_sum_threshold <bytes>
  ```

  

* CACHE_POOL_NEAR_FULL

  ```
  ceph osd pool set <cache-pool-name> target_max_bytes <bytes>
  ceph osd pool set <cache-pool-name> target_max_objects <objects>
  ```

  

* TOO_FEW_PGS

* POOL_PG_NUM_NOT_POWER_OF_TWO

  ```
  ceph osd pool set <pool-name> pg_num <value>
  ceph config set global mon_warn_on_pool_pg_num_not_power_of_two false
  ```

* POOL_TOO_FEW_PGS

  ```
  ceph osd pool set <pool-name> pg_autoscale_mode off
  
  ceph osd pool set <pool-name> pg_autoscale_mode on
  
  ceph osd pool set <pool-name> pg_num <new-pg-num>
  ```

  

* TOO_MANY_PGS

  ```
  ceph osd in <osd id(s)>
  ```

  

* POOL_TOO_MANY_PGS

  ```
  ceph osd pool set <pool-name> pg_autoscale_mode off
  ceph osd pool set <pool-name> pg_autoscale_mode on
  ceph osd pool set <pool-name> pg_num <new-pg-num>
  ```

  

* POOL_TARGET_SIZE_BYTES_OVERCOMMITTED

  ```
  ceph osd pool set <pool-name> target_size_bytes 0
  ```

  

* POOL_HAS_TARGET_SIZE_BYTES_AND_RATIO

  ```
  ceph osd pool set <pool-name> target_size_bytes 0
  ```

  

* TOO_FEW_OSDS

* SMALLER_PGP_NUM

  ```
  ceph osd pool set <pool> pgp_num <pg-num-value>
  ```

  

* MANY_OBJECTS_PER_PG

* POOL_APP_NOT_ENABLED

  ```
  rbd pool init <poolname>
  ceph osd pool application enable foo
  ```

  

* POOL_FULL

  ```
  ceph osd pool set-quota <pool> max_bytes <bytes>
  ceph osd pool set-quota <pool> max_objects <objects>
  ```

  

* POOL_NEAR_FULL

  ```
  ceph osd pool set-quota <pool> max_bytes <bytes>
  ceph osd pool set-quota <pool> max_objects <objects>
  ```

  

* OBJECT_MISPLACED

* OBJECT_UNFOUND

  ```
  ceph tell <pgid> query
  ```

  

* SLOW_OPS

  ```
  ceph daemon osd.<id> ops
  ceph daemon osd.<id> dump_historic_ops
  ceph osd find osd.<id>
  ```

  

* PG_NOT_SCRUBBED

  ```
  ceph pg scrub <pgid>
  ```

  

* PG_NOT_DEEP_SCRUBBED

  ```
  ceph pg deep-scrub <pgid>
  ```

  

### MISCELLANEOUS

* RECENT_CRASH

  ```
  ceph crash ls-new
  ceph crash info <crash-id>
  ceph crash archive <crash-id>
  ceph crash archive-all
  ceph config set mgr/crash/warn_recent_interval 0
  ```

  

  

* TELEMETRY_CHANGED

  ```
  ceph telemetry show
  ceph telemetry on
  ceph telemetry off
  ```

## PG

* 一个pg是若干对象的逻辑集合，为了保证数据可靠性，这些对象被复制到多个osd上，根据存储池副本级别，每个pg会被复制和分布到ceph集群中多个osd上，可以认为pg是包含多个对象的逻辑容器，该容器被映射到多个osd
* 增加一个集群的pg数目会导致集群重新平衡osd负载
* 建议每个osd对应的pg数目在50到100之间，这样可以减少osd节点上资源消耗
* 当ceph集群内数据增加时，可能需要通过增加pg数目对集群做适当的调节
* 当设备被加入集群或从集群删除时，crush会通过最优化的方式来管理pg迁移(relocation)
* osd被分为主（primary）、从（secondary）、再从（tertiary），它们都在pg的acting set中

### PG状态

* Creating(创建中): pg正在被创建 。通常当存储池正在被创建或增加一个存储池的pg数目时，pg会呈现这种状态
* Active(活动的): pg是活动的，可以被读写。该pg操作请求都将被处理
* Clean(清洁的): pg中的所有对象都已被复制了规定的份数
* Down(失效的): 包含pg必须数据的一个副本实效（down）了，因此pg是离线的(down)
* Replay(重做): 某osd崩溃后pg正在等待客户端重新发起操作
* Splitting(分割中): pg正在被分割为多个pg。该状态通常一个存储池的pg数增加后呈现。比如增加pg数目后，已有的pg将会被分割，它们部分对象会被移动到新的pg上
* Scrubbing(清理中): pg正在做不一致性校验
* Degraded(降级的): pg中部分对象的副本数未达到规定数目
* inconsistent(不一致的): pg的副本出现了不一致。对象大小不正确，或者恢复(recovery)结束后某副本出现了对象丢失的情形
* Peering(对等互联中): pg正处于peering过程中。peering是由主osd发起的，让存放pg副本的所有osd就pg的所有对象和元数据的状态达成一致的过程。peering过程完成后，主osd就能接受客户端写请求了
* Repair(修复中): pg正在被检查，被发现的任何不一致都将尽可能的被修复
* Recovering(恢复中): pg正在迁移或同步对象及副本。一个osd停止服务(down)后，其内容版本将会落后于pg内的其他副本，，这时pg就会进入该状态，该osd上的对象将被从其他副本迁移或同步过来
* Backfill(回填): 一个新osd加入集群后，crush会把集群现有的一部分pg分配给它
* Backfill-wait(回填-等待): pg正在等待开始回填操作
* Incomplete(不完整的): pg日志中缺失了一关键时间段的数据。当包含pg所需信息的某osd实效或者不可用之后，往往会出现这种情况
* Stale(陈旧的): pg处于未知状态, monitors在pg map改变后还没受到过pg的更新。启用一个集群后，常常会看到在peering过程结束前pg处于该状态
* Remapped(重映射):当pg的acting set变化后，数据将会从旧acting set迁移到新action set。新主osd需要过一段时间后才能提供服务。因此，它会让老的主osd继续提供服务,直到pg迁移完成。数据迁移完成后，pg map将使用新acting set中的主osd


## crush map

### crush map4个主要段

1. **设备：**由任意对象存储设备组成，即对应一个 `ceph-osd`进程的存储器。 Ceph 配置文件里的每个 OSD 都应该有一个设备。

   ```
   # CRUSH Map 需要 OSD 列表（即配置文件所定义的 OSD 守护进程名称）
   # devices
   device {num} {osd.name}
   
   # devices例子
   device 0 osd.0
   device 1 osd.1
   device 2 osd.2
   device 3 osd.3
   ```
   

   
2. **桶类型：** 定义了 CRUSH 分级结构里要用的桶类型（ `types` ），桶由逐级汇聚的存储位置（如行、机柜、机箱、主机等等）及其权组成。

   ```
   # types
   type {num} {bucket-name}
   
   # types例子
   type 0 osd
   type 1 host
   type 2 chassis
   type 3 rack
   type 4 row
   type 5 pdu
   type 6 pod
   type 7 room
   type 8 datacenter
   type 9 region
   type 10 root
   ```

   

3. **桶实例：** 定义了桶类型后，还必须声明主机的桶类型、以及规划的其它故障域。

   ```
   [bucket-type] [bucket-name] {
           id [a unique negative numeric ID]
           weight [the relative capacity/capability of the item(s)]
           alg [the bucket type: uniform | list | tree | straw ]
           hash [the hash type: 0 by default]
           item [item-name] weight [weight]
   }
   
   
   #
   host node1 {
           id -1
           alg straw
           hash 0
           item osd.0 weight 1.00
           item osd.1 weight 1.00
   }
   
   host node2 {
           id -2
           alg straw
           hash 0
           item osd.2 weight 1.00
           item osd.3 weight 1.00
   }
   
   rack rack1 {
           id -3
              alg straw
           hash 0
           item node1 weight 2.00
           item node2 weight 2.00
   }
   Ceph 用双精度类型数据表示桶权重。权重和设备容量不同，我们建议用 1.00 作为 1TB 存储设备的相对权重，这样 0.5 的权重大概代表 500GB 、 3.00 大概代表 3TB 。较高级桶的权重是所有叶子桶的权重之和。
   一个桶的权重是一维的，你也可以计算条目权重来反映存储设备性能。例如，如果你有很多 1TB 的硬盘，其中一些数据传输速率相对低、其他的数据传输率相对高，即使它们容量相同，也应该设置不同的权重（如给吞吐量较低的硬盘设置权重 0.8 ，较高的设置 1.20 ）。
   ```

4. **规则：** 由选择桶的方法组成。

   * CRUSH Map 支持“ CRUSH 规则”的概念，用以确定一个存储池里数据的分布
   * CRUSH 规则定义了归置和复制策略、或分布策略，用它可以规定 CRUSH 如何放置对象副本
   * 对大型集群来说，你可能创建很多存储池，且每个存储池都有它自己的 CRUSH 规则集和规则。默认的 CRUSH Map 里，每个存储池有一条规则、一个规则集被分配到每个默认存储池。
   *  大多数情况下，你都不需要修改默认规则。新创建存储池的默认规则集是 `0` 

   ```
   rule <rulename> {
           ruleset <ruleset>
           type [ replicated | erasure ]
           min_size <min-size>
           max_size <max-size>
           step take <bucket-type>
           step [choose|chooseleaf] [firstn|indep] <N> <bucket-type>
           step emit
   }
   
   参数说明：
   ruleset：区分一条规则属于某个规则集的手段。给存储池设置规则集后激活。
   
   type：规则类型，目前仅支持 replicated 和 erasure ，默认是 replicated 。
   
   min_size：可以选择此规则的存储池最小副本数。
   
   max_size：可以选择此规则的存储池最大副本数。
   
   step take <bucket-name>：选取起始的桶名，并迭代到树底。
   
   step choose firstn {num} type {bucket-type}：选取指定类
   型桶的数量，这个数字通常是存储池的副本数（即 pool size ）。如果 {num} == 0 ， 选择 pool-num-replicas 个桶（所有可用的）；如果 {num} > 0 && < pool-num-replicas ，就选择那么多的桶；如果 {num} < 0 ，它意味着选择 pool-num-replicas - {num} 个桶。
   
   step chooseleaf firstn {num} type {bucket-type}：选择 {bucket-type} 类型的桶集合，并从各桶的子树里选择一个叶子节点。桶集合的数量通常是存储池的副本数（即 pool size ）。如果 {num} == 0 ，选择 pool-num-replicas 个桶（所有可用的）；如果 {num} > 0 && < pool-num-replicas ，就选择那么多的桶；如果 {num} < 0 ，它意味着选择 pool-num-replicas - {num} 个桶。
   
   step emit：输出当前值并清空堆栈。通常用于规则末尾，也适用于相同规则应用到不同树的情况。
   ```

### 主亲和性

某个 Ceph 客户端读写数据时，总是连接 acting set 里的主 OSD （如 `[2, 3, 4]` 中， `osd.2` 是主的）。有时候某个 OSD 与其它的相比并不适合做主 OSD （比如其硬盘慢、或控制器慢）。最大化硬件利用率时为防止性能瓶颈（特别是读操作），你可以调整 OSD 的主亲和性，这样 CRUSH 就尽量不把它用作 acting set 里的主 OSD 了。

```
ceph osd primary-affinity <osd-id> <weight>
```

### 增加桶

要在运行集群的 CRUSH Map 中新建一个桶，用 ceph osd crush add-bucket 命令：

```
ceph osd crush add-bucket {bucket-name} {bucket-type}
```

### 移动桶0

要把一个桶移动到 CRUSH Map 里的不同位置，执行命令：

```
ceph osd crush move {bucket-name} {bucket-type}={bucket-name} [{bucket-type}={bucket-name} ...]
```

### 删除桶

要把一个桶从 CRUSH Map 的分级结构中删除，可用此命令：

```
ceph osd crush remove {bucket-name}
```

注意：从 CRUSH 分级结构里删除时必须是空桶。


# 相关文章


## PG

转载自：https://www.jianshu.com/p/36c2d5682d87

### 介绍
* 架构层次上，pg位于rados层中间
a. 往上负责接收和处理来自客户端的请求。
b. 往下负责将这些数据请求翻译为能够被本地对象存储所能理解的事务。
* 是组成存储池的基本单位，存储池中的很多特性，都是直接依托于PG实现的。
* 面向容灾域的备份策略使得一般而言的PG需要执行跨节点的分布式写，因此数据在不同节点之间的同步、恢复时的数据修复也都是依赖PG完成。

### 状态表

* 正常的PG状态是 100%的active + clean， 这表示所有的PG是可访问的，所有副本都对全部PG都可用。

| 状态 | 描述 |
| ---- | ---- |
|   Activating   | Peering已经完成，PG正在等待所有PG实例同步并固化Peering的结果(Info、Log等)     |
| Active | 活跃态。PG可以正常处理来自客户端的读写请求 |
|  |  |
| Backfilling | 正在后台填充态。 backfill是recovery的一种特殊场景，指peering完成后，如果基于当前权威日志无法对Up Set当中的某些PG实例实施增量同步(例如承载这些PG实例的OSD离线太久，或者是新的OSD加入集群导致的PG实例整体迁移) 则通过完全拷贝当前Primary所有对象的方式进行全量同步 |
| Backfill-toofull | 某个需要被Backfill的PG实例，其所在的OSD可用空间不足，Backfill流程当前被挂起 |
| Backfill-wait | 等待Backfill 资源预留 |
| Clean | 干净态。PG当前不存在待修复的对象， Acting Set和Up Set内容一致，并且大小等于存储池的副本数 |
| Creating | PG正在被创建 |
|  |  |
| Deep | PG正在或者即将进行对象一致性扫描清洗 |
|  |  |
| Degraded | 降级状态。Peering完成后，PG检测到任意一个PG实例存在不一致(需要被同步/修复)的对象，或者当前ActingSet 小于存储池副本数 |
| Down | Peering过程中，PG检测到某个不能被跳过的Interval中(例如该Interval期间，PG完成了Peering，并且成功切换至Active状态，从而有可能正常处理了来自客户端的读写请求),当前剩余在线的OSD不足以完成数据修复 |
| Incomplete | Peering过程中， 由于 a. 无非选出权威日志 b. 通过choose_acting选出的Acting Set后续不足以完成数据修复，导致Peering无非正常完成 |
| Inconsistent | Peering过程中， 由于 a. 无非选出权威日志 b. 通过choose_acting选出的Acting Set后续不足以完成数据修复，导致Peering无非正常完成 |
| Inconsistent | Peering过程中， 由于 a. 无非选出权威日志 b. 通过choose_acting选出的Acting Set后续不足以完成数据修复，导致Peering无非正常完成 |
| Peered | Peering已经完成，但是PG当前ActingSet规模小于存储池规定的最小副本数(min_size) |
| Peering | 正在同步态。PG正在执行同步处理 |
| Recovering | 正在恢复态。集群正在执行迁移或同步对象和他们的副本 |
| Recovering-wait | 等待Recovery资源预留 |
| Remapped | 重新映射态。PG活动集任何的一个改变，数据发生从老活动集到新活动集的迁移。在迁移期间还是用老的活动集中的主OSD处理客户端请求，一旦迁移完成新活动集中的主OSD开始处理 |
| Repair | PG在执行Scrub过程中，如果发现存在不一致的对象，并且能够修复，则自动进行修复状态 |
| Scrubbing | PG正在或者即将进行对象一致性扫描 |
| Unactive | 非活跃态。PG不能处理读写请求 |
| Unclean | 非干净态。PG不能从上一个失败中恢复 |
| Stale | 未刷新态。PG状态没有被任何OSD更新，这说明所有存储这个PG的OSD可能挂掉, 或者Mon没有检测到Primary统计信息(网络抖动) |
|  Undersized | PG当前Acting Set小于存储池副本数 |

### 故障模拟

#### Degraded

降级：由上文可以得知，每个PG有三个副本，分别保存在不同的OSD中，在非故障情况下，这个PG是active+clean 状态，那么，如果PG 的 副本osd.4 挂掉了，这个 PG 是降级状态。

为了模拟故障，(size = 3, min_size = 2) 我们手动停止了 osd.1，然后查看PG状态，可见，它此刻的状态是active+undersized+degraded,当一个 PG 所在的 OSD 挂掉之后，这个 PG 就会进入undersized+degraded 状态，而后面的[0,2]的意义就是还有两个副本存活在 osd.0 和 osd.2 上, 并且这个时候客户端可以正常读写IO。

降级就是在发生了一些故障比如OSD挂掉之后，Ceph 将这个 OSD 上的所有 PG 标记为 Degraded。

降级的集群可以正常读写数据，降级的 PG 只是相当于小毛病而已，并不是严重的问题。

Undersized的意思就是当前存活的PG 副本数为 2，小于副本数3，将其做此标记，表明存货副本数不足，也不是严重的问题。

#### Peered

Peering已经完成，但是PG当前Acting Set规模小于存储池规定的最小副本数(min_size)。

在 OSD 挂掉或者在扩容的时候PG 上的OSD会按照Crush算法重新分配PG 所属的osd编号。并且会把 PG  Remap到别的OSD上去。

Remapped状态时，PG当前Acting Set与Up Set不一致。

客户端IO可以正常读写。

#### recovery
指PG通过PGLog日志针对数据不一致的对象进行同步和修复的过程。
Recovery是通过记录的PGLog进行恢复数据的。
记录的PGLog 在osd_max_pg_log_entries=10000条以内，这个时候通过PGLog就能增量恢复数据。

#### Backfill
当PG的副本无非通过PGLog来恢复数据，这个时候就需要进行全量同步，通过完全拷贝当前Primary所有对象的方式进行全量同步。
无法根据记录的PGLog进行恢复数据时，就需要执行Backfill过程全量恢复数据。
如果超过osd_max_pg_log_entries=10000条， 这个时候需要全量恢复数据。

#### Stale
mon检测到当前PG的Primary所在的osd宕机。
Primary超时未向mon上报pg相关的信息(例如网络阻塞)。
PG内三个副本都挂掉的情况。
先停止同一个PG内两个副本，状态是undersized+degraded+peered。
然后停止同一个PG内三个副本，状态是stale+undersized+degraded+peered。

#### inconsistent
PG通过Scrub检测到某个或者某些对象在PG实例间出现了不一致
当PG内部三个副本有数据不一致的情况，想要修复不一致的数据文件，只需要执行ceph pg repair修复指令，ceph就会从其他的副本中将丢失的文件拷贝过来就行修复数据。
当osd短暂挂掉的时候，因为集群内还存在着两个副本，是可以正常写入的，但是 osd.34 内的数据并没有得到更新，过了一会osd.34上线了，这个时候osd.34的数据是陈旧的，就通过其他的OSD 向 osd.34 进行数据的恢复，使其数据为最新的，而这个恢复的过程中，PG的状态会从inconsistent ->recover -> clean,最终恢复正常。
这是集群故障自愈一种场景。

#### Down
Peering过程中，PG检测到某个不能被跳过的Interval中(例如该Interval期间，PG完成了Peering，并且成功切换至Active状态，从而有可能正常处理了来自客户端的读写请求),当前剩余在线的OSD不足以完成数据修复
首先有一个PG 3.7f有三个副本[5,21,29]， 当停掉一个osd.21之后， 写入数据到osd.5, osd.29。 这个时候停掉osd.29, osd.5 ，最后拉起osd.21。 这个时候osd.21的数据比较旧，就会出现PG为down的情况，这个时候客户端IO会夯住，只能拉起挂掉的osd才能修复问题。

#### PG为Down的OSD丢失或无法拉起 
a. 删除无法拉起的OSD
b. 创建对应编号的OSD
c. PG的Down状态就会消失
d. 对于unfound 的PG ，可以选择delete或者revert 
ceph pg {pg-id} mark_unfound_lost revert|delete

#### Incomplete
Peering过程中， 由于 a. 无非选出权威日志 b. 通过choose_acting选出的Acting Set后续不足以完成数据修复，导致Peering无非正常完成。
常见于ceph集群在peering状态下，来回重启服务器，或者掉电。



## 如何指定ssd盘为主osd，hdd为从osd
转载自：https://blog.csdn.net/wylfengyujiancheng/article/details/88299416
```
client 
	---write--->  primary osd 
		---write---> secondary osd / tertiary osd 
	--- ack ---> primary osd 
---ack--->client
```

CEPH 会认为每个磁盘能力一样 会平均每个磁盘的 IO 能力(primary affinity)与存储空间能力(weight) primary affinity 参数可以在不改变磁盘存储能力的情况下, 降低在 CEPH 集群使用过程中对该 osd 的认购 primary pg 能力。
```
[mon]
mon_osd_allow_primary_affinity = true
修改完重启ceph-mon

ceph osd primary-affinity osd.4 0.5

修改权限之前
ceph pg dump | grep active+clean  | grep '\[4' | wc -l
48
修改权重之后
ceph pg dump | grep active+clean  | grep '\[4' | wc -l
8
```

## CRUSH TUNABLES
转载自:  https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/1.2.3/html/storage_strategies/crush_tunables

### tunables profile

* legacy: the legacy behavior from v0.47 (pre-Argonaut) and earlier.
* argonaut: the legacy values supported by v0.48 (Argonaut) release.
* bobtail: the values supported by the v0.56 (Bobtail) release.
* firefly: the values supported by the v0.80 (Firefly) release.
* optimal: the current best values.
* default: the current default values for a new cluster.


## Ceph的稳定性与性能调优
转载自: http://gcs.truan.wang/portfolio/ceph-stability-and-performance-tuning/

## ceph节点journal盘更换
转载自: https://www.codercto.com/a/92393.html

## 硬盘容量不均衡导致的缓存盘寿命急速衰减分析
转载自: https://www.zstack.io/m/view.php?aid=1765

