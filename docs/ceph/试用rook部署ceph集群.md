# 试用rook v1.2.7的记录


## rook总结
### 准备好虚机并安装好kubernetes
* 虚机数量随意
* 测试示例用系统盘当osd，可以不额外加硬盘
* rook官方文档关于k8s集群需求说明：https://rook.io/docs/rook/v1.2/k8s-pre-reqs.html

### 下载ceph集群示例
```
git clone --single-branch --branch release-1.2 https://github.com/rook/rook.git
cd cluster/examples/kubernetes/ceph
```
    其中部署集群需要用到的文件 
    common.yaml	配置信息
    operator.yaml	配置rook-operator
    cluster-test.yaml	配置测试集群示例

### 修改配置文件
* operator.yaml
  如果要测试ceph-csi，国内可能要修改csi的镜像拉取地址

* cluster-test.yaml
<div class="note info"><p>配置说明和例子：https://rook.io/docs/rook/v1.2/ceph-cluster-crd.html</p></div>

  测试用的配置文件默认将系统盘当成一个osd，用的filestore模式，配置项为directories,该配置方法只能用在**测试环境**

### 部署
```
kubectl create -f common.yaml
kubectl create -f operator.yaml
kubectl create -f cluster-test.yaml
```

### toolbox执行ceph命令

* 安装toolbox

  ```
  kubectl create -f toolbox.yaml
  ```

* 查看toolbox状态

  ```
  kubectl -n rook-ceph get pod -l "app=rook-ceph-tools"
  ```

* 进入toolbox容器

  ```
  kubectl -n rook-ceph exec -it $(kubectl -n rook-ceph get pod -l "app=rook-ceph-tools" -o jsonpath='{.items[0].metadata.name}') bash
  ```

  进入toolbox后就可以执行ceph相关命令了

### 清理ceph测试集群
> 参考：https://rook.io/docs/rook/v1.2/ceph-teardown.html

* cluster/examples/kubernetes/ceph文件夹中执行
```
kubectl delete -f ../wordpress.yaml
kubectl delete -f ../mysql.yaml
kubectl delete -n rook-ceph cephblockpool replicapool
kubectl delete storageclass rook-ceph-block
kubectl delete -f csi/cephfs/kube-registry.yaml
kubectl delete storageclass csi-cephfs
```

* 删除CephCluster
```
kubectl -n rook-ceph delete cephcluster rook-ceph
#确认已经删除
kubectl -n rook-ceph get cephcluster 
```

* 删除common和operator

```
kubectl delete -f operator.yaml
kubectl delete -f common.yaml
```

* 删除目录和清理osd磁盘
  删除dataDirHostPath指定的目录，如：

  ```
  rm -rf /var/lib/rook
  ```

* 清理磁盘
  
```
  #!/usr/bin/env bash
  DISK="/dev/sdb"
  # Zap the disk to a fresh, usable state (zap-all is important, b/c MBR has to be clean)
  # You will have to run this step for all disks.
  sgdisk --zap-all $DISK
  
  # These steps only have to be run once on each node
  # If rook sets up osds using ceph-volume, teardown leaves some devices mapped that lock the disks.
  ls /dev/mapper/ceph-* | xargs -I% -- dmsetup remove %
  # ceph-volume setup can leave ceph-<UUID> directories in /dev (unnecessary clutter)
  rm -rf /dev/ceph-*
```
  
## ceph管理
### osd管理

#### 增加osd

rook会根据配置自动添加osd

#### 删除osd

rook默认不会自动删除osd。
删除前需要确认：

* 有足够的剩余空间删除osd
* 确认剩余osd和pg是健康状态能够均衡数据
* 一次不要删除过多osd
* 删除多个osd时，每次等均衡完成再删下一个

toolbox中删除操作：

1. 确认osd的id

2. 设置osd为out
   ```
   ceph osd out osd.<ID>
   ```
   
3. 等待数据均衡完成
4. 移除硬盘

5. 更新cluster.yaml里面配置，让这个硬盘不会再次自动加如集群

6. 移除osd
   ```
   ceph osd purge <ID> --yes-i-really-mean-it
   ```

7. 确认osd已经删除
   ```
   ceph osd tree
   ```

k8s中移除osd的deployment：

​	配置文件中可以开启自动移除out状态的osd

```
removeOSDsIfOutAndSafeToRemove: true
```
​	如果没有开启自动删除，可以手动删除

```
kubectl delete deployment -n rook-ceph rook-ceph-osd-<ID>
```

最后参考清理磁盘脚本初始化磁盘

#### 替换osd

1. 执行删除osd步骤
2. 替换硬盘
3. 更新cluster.yaml确保硬盘可以被自动添加
4. 等待自动添加
5. 执行ceph osd tree查看添加结果
