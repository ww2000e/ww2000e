# tmux cheatsheet

## 无配置使用tmux

###  tmux快捷键

>ctrl+b 进入指令模式 ? 查看帮助

#### tmux进程/会话 操作
| ctrl+b后按键 | 功能 |
| --- | --- |
| d | 后台运行当前tmux进程, 执行tmux attach恢复 |
| ctrl+z | 暂停当前tmux进程至后台, 执行fg或者tmux attach恢复 |
| $ | 重命名当前会话 |
| (    ) | 选择上一个/下一个 tmux进程 |
| D | 选择一个tmux进程 |
| L | 切换tmux进程 |
| r | 刷新tmux |

#### 窗口操作
| ctrl+b后按键 | 功能 |
| --- | --- |
| c | 新建窗口 |
| " | 水平分割窗口 |
| % | 垂直分割窗口 |
| ! | 当前窗格新建一个窗口 |
| & | 关闭当前窗口 |
| ' | 输入窗口号选择窗口 |
| , | 重命名当前窗口 |
| . | 指定当前窗口一个新的序号 |
| 0 ~ 9 | 按数字键选择窗口 |
| w | 选择窗口 |
| f | 查找窗口 |
| l | 最后一个窗口 |
| n | 下一个窗口 |
| p | 上一个窗口 |

#### 窗格操作
| ctrl+b后按键 | 功能 |
| --- | --- |
| ctrl+o | 逆时针旋转当前窗口中窗格 |
| space | 当前窗口中窗格下一个布局 |
| ; | 选择最后的窗格 |
| [ | 复制模式 |
| ctrl+space | 默认emacs模式，复制模式中开始选择 |
| alt+w | 默认emacs模式，复制模式中选择完成 |
| ] | 粘贴模式 |
| i | 显示当前窗格信息 |
| o | 切换到下一个窗格 |
| q | 显示窗格标号 |
| s | 窗格树中选择窗格 |
| t | 整个窗格显示时间 |
| x | 关闭当前窗格 |
| z | 开关显示当前窗格 |
| {    } | 交换当前窗格 |
| 方向键Up | 选择上边窗格 |
| 方向键Down | 选择下边窗格 |
| 方向键Left | 选择左边窗格 |
| 方向键Right | 选择右边窗格 |
| alt+1 | 所有窗格变为垂直分割 |
| alt+2 | 所有窗格变为水平分割 |
| alt+3 | 一个主要水平分割窗格，其他垂直分割 |
| alt+4 | 一个主要垂直分割窗格，其他水平分割 |
| alt+5 | 均与分割所有窗格 |
| alt+o | 旋转当前窗口中的窗格 |
| alt+方向键 | 改变窗格大小 |
| ctrl+方向键 | 微调当前窗格大小 |

#### 其他
| ctrl+b后按键 | 功能 |
| --- | --- |
| : | 命令行 |
| # | list buffers |
| - | delete-buffer |
| = | choose buffer |
| ~ | show messages |
| alt+n | next-window |
| alt+p | previous-window |

#### 命令
ctrl + b后按":"冒号

| 命令 | 作用 |
| --- | --- |
| move-window -t 0 | 窗口移动到0号 |
| swap-window -t 0 | 当前窗口与0号对调 |
| swap-window -s 3 -t 1 | 3号与1号对调 |

## 配置使用tmux
### 1.9a版本
每次打开窗口都在相同目录的配置
```
bind-key c new-window -c '#{pane_current_path}'
bind-key '"' split-window -c '#{pane_current_path}'
bind-key % split-window -h -c '#{pane_current_path}'
```
