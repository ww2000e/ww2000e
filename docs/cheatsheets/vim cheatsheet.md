# vim cheatsheet
## 无插件vim使用
### vim快捷键
#### ~/.vimrc配置
``` vimrc
set number

set cc=80

set cursorline

set history=2000

set tabstop=4
set shiftwidth=4
set expandtab
set softtabstop=2
syntax on
filetype indent on

set laststatus=2

set showmatch
set hlsearch

set undofile
set backupdir=~/.vim/.backup//
set directory=~/.vim/.swp//
set undodir=~/.vim/.undo//

set visualbell

set autoread

set statusline=%t[%{strlen(&fenc)?&fenc:'none'},%{&ff}]%h%m%r%y%=%c,%l/%L\ %P   

set list
set listchars=tab:›\ ,trail:•,extends:#,nbsp:. 

autocmd FileType javascript setlocal sw=2 ts=2 sts=2
autocmd FileType go setlocal et!

augroup emberhbs
    au!
    au BufRead,BufNewFile,BufEnter      *.hbs   set sw=2 ts=2 sts=2
    au BufRead,BufNewFile,BufEnter      *.hbs   setfiletype html
augroup END

```

#### 方向移动
| 按键 | 功能 |
| --- | --- |
| w,b | 左右移到一个单词，光标停在下一个单词开头 |
| W,B | 左右移到下一个单词开头，但忽略一些标点 |
| e,ge | 左右移一个单词，光标停在下一个单词末尾 |
| E,gE | 左右移一个单词末尾，如果词尾有标点，则移动到标点 |
| (,) | 左右移动一句 |
| {,} | 左右移动一段 |
| f{char},F{char} | 左右移动到{char}表示的字母处 |
| 0, g0 | 移动到行首， 移动到光标所在屏幕行首 |
| ^, g^ | 移动到本行/移动当前屏幕第一个非空白字符处 |
| ?&#124;, ?G | 移动到？列，移动到？行 |
| H, M, L | 把光标移动到屏幕最顶端/中间/最底/一行 |
| gg, G | 文件头部/尾部 |
| ctrl+f, ctrl+b | 上/下翻一屏 |
| ctrl+d, ctrl+u | 上/下翻半屏 |
| ctrl+e, ctrl+y | 上/下滚动一行 |
| zz, zt, zb | 当前行移动到屏幕中间/顶端/底端 |

#### 插入状态下操作
| 按键 | 功能 |
| --- | --- |
| ctrl+h | 删除前一个字符(退格键) |
| ctrl+r, 0 | 粘贴寄存器0内容 |
| ctrl+r, ctrl+p, 0 | 粘贴寄存器0内容，更加智能 |


#### 操作
| 按键 | 功能 |
| --- | --- |
| A | 本行结尾处插入 |
| . | 重复上一次动作 |
| ; | 下一个查找目标 f{char} |
| O | 上面增加一行 |
| C | 删除光标位置到结尾 |
| I | 开头第一个字符处插入 |
| ctrl+[ | 回到普通模式 |
| ctrl+d |  命令模式自动补全 |
| ctrl+r ctrl+w | 当前单词粘贴到命令行 |
| ctrl+r " | 将y复制的内容复制到vim命令行 |
| ctrl+w s,v | 水平 ，垂直分隔 |
| clo[se] | 关闭活动窗口 |
| on[ly] | 只保留活动窗口，关闭其他所有窗口 |
| :changes | 查看改动 |
| :bufdo tab split | buffer转成tab |

#### 多标签编辑
| 按键 | 功能 |
| --- | --- |
| vim -p files | 打开多个文件，每个文件占用一个标签页 |
| :tabe, tabnew | 如果加文件名，就在新的标签中打开这个文件， 否则打开一个空缓冲区 |
| ^w gf | 在新的标签页里打开光标下路径指定的文件 |
| :tabn | 切换到下一个标签。ctrl+PageDown，也可以 |
| :tabp | 切换到上一个标签。ctrl+PageUp，也可以 |
| [n] gt | 切换到下一个标签。如果前面加了 n ，就切换到第n个标签。第一个标签的序号就是1 |
| :tab spliti | 将当前缓冲区的内容在新页签中打开 |
| :tabc[lose] | 关闭当前的标签页 |
| :tabo[nly]  | 关闭其它的标签页 |
| :tabs | 列出所有的标签页和它们包含的窗口 |
| :tabm[ove] [N] | 移动标签页，移动到第N个标签页之后。 如 tabm 0 当前标签页，就会变成第一个标签页 |

#### 分屏
| 按键 | 功能 |
| --- | --- |
| vim -o file1 file2 | 水平分割窗口，同时打开file1和file2 |
| vim -O file1 file2 | 垂直分割窗口，同时打开file1和file2 |
| vim  -o5 file1  file2 | 将分配5个相同的窗口，有3个是闲置的 |
| :sp[lit] | 把当前窗水平分割成两个窗口。(ctrl+W s 或 ctrl+W ctrl+S) 注意如果在终端下，ctrl+S可能会冻结终端，请按ctrl+Q继续 |
| :vsplit | 创建全新的垂直分割的窗口，同样是显示和当前窗口同一个文件内容 |
| :split filename | 水平分割窗口，并在新窗口中显示另一个文件 |
| :nsplit(:nsp) | 水平分割出一个n行高的窗口 |
| :[N]new | 水平分割出一个N行高的窗口，并编辑一个新文件。 (ctrl+W n或 ctrl+W ctrl+N) |
| ctrl+w f | 水平分割出一个窗口，并在新窗口打开名称为光标所在词的文件 |
| ctrl+w ctrl+^ | 水平分割一个窗口，打开刚才编辑的文件 |
| vert sb [n] | 当前位置垂直分割打开buffer序号为n的文件 |
| vert blowright sb [n] | 最右边位置垂直分割打开buffer序号为n的文件 |

#### 关闭多个窗口
| 按键 | 功能 |
| --- | --- |
| :qall | 关闭所有窗口，退出vim |
| :wall | 保存所有修改过的窗口 |
| :only | 只保留当前窗口，关闭其它窗口。(ctrl+Wo) |
| :close | 关闭当前窗口，ctrl+W c能实现同样的功能。 (象 :q :x同样工作 ) |

#### 调整窗口大小
| 按键 | 功能 |
| --- | --- |
| ctrl+W + | 当前窗口增高一行。也可以用n增高n行 |
| ctrl+W - | 当前窗口减小一行。也可以用n减小n行 |
| ctrl+W _ | 当前窗口扩展到尽可能的大。也可以用n设定行数 |
| :resize n | 当前窗口n行高 |
| ctrl+W = | 所有窗口同样高度 |
| n ctrl+W _ | 当前窗口的高度设定为n行 |
| ctrl+W < | 当前窗口减少一列。也可以用n减少n列 |
| ctrl+W > | 当前窗口增宽一列。也可以用n增宽n列 |
| ctrl+W &#124; | 当前窗口尽可能的宽。也可以用n设定列数 |

#### 切换移动窗口
| 按键 | 功能 |
| --- | --- |
| ctrl+w ctrl+w |  切换到下一个窗口。或者是ctrl+w w |
| ctrl+w p | 切换到前一个窗口 |
| ctrl+w h(l,j,k) | 切换到左（右，下，上）的窗口 |
| ctrl+w t(b) | 切换到最上（下）面的窗口 |
| ctrl+w H(L,K,J) | 将当前窗口移动到最左（右、上、下）面 |
| ctrl+w r | 旋转窗口的位置 |
| ctrl+w T | 将当前的窗口移动到新的标签页上 |

#### 自动补全
##### 单词自动补全
| 按键 | 功能 |
| --- | --- |
| ctrl+n | 当你输入第一个字母的时候，再ctrl+n，自动出现下拉菜单，单词默认选中第一个，继续ctrl+n，ctrl+p可以上下切换，或者用方向键（太慢） |
| ctrl+p | 同上，只是默认的选中的是列表中最后一个单词 |

##### 行自动补全
| 按键 | 功能 |
| --- | --- |
| ctrl+x ctrl+l | 两个命令组合使用。在插入模式下输入已经存在行的第一个单词，再按这两个键，就会列出该整行出来 |

##### 文件名自动补全
| 按键 | 功能 |
| --- | --- |
| ctrl+x ctrl+f | 插入模式下，按这两个组合键，可以插入当前目录下的文件名。处用在哪里呢，当然是有时候我们要指定默认执行文件的路径，这样就方便啦 |

##### 文件浏览
| 按键 | 功能 |
| --- | --- |
| :e path | 打开目录浏览，“.”为当前目录 |
| - | 返回上级目录 |
| c | 切换vim的当前工作目录为正在浏览的目录 |
| d | 创建目录 |
| D | 删除文件或目录 |
| i | 切换显示方式 |
| R | 改名文件或目录 |
| s | 选择排序方式 |
| x | 定制浏览方式，使用你指定的程序打开该文件 |
| o | 水平打开选择的文件 |
| v | 垂直打开选择的文件 |
| ctrl+^ | 目录和文件间切换 |

#### vimgrep
| 按键 | 功能 |
| --- | --- |
| g | 表示是否把每一行的多个匹配结果都加入 |
| j | 表示是否搜索完后定位到第一个匹配位置 |
| vimgrep /pattern/ % | 在当前打开文件中查找 |
| vimgrep /pattern/ * | 在当前目录下查找所有 |
| vimgrep /pattern/ ** | 在当前目录及子目录下查找所有 |
| vimgrep /pattern/ *.c  |  查找当前目录下所有.c文件 |
| vimgrep /pattern/ **/* | 只查找子目录 |
| cn | 查找下一个 |
| cp | 查找上一个 |
| copen | 打开quickfix |
| cw | 打开quickfix |
| cclose | 关闭qucikfix |

### vimdiff快捷键
| 功能 | 按键 |
| --- | --- |
| 不需要滚动同步 | :set noscrollbind |
| 跳到差异点 | ]c |
| 当前差异合并去另一个文件 | dp |
| 从另一个文件合并差异 | do |
| 撤销修改 | 在改动窗口中 :u |
| 刷新比较结果 | :diffupdate |
| 跳转窗口 | ctrl+w,w |


### 打开压缩文件编辑
```
vim archive.tar.gz
```
支持tar.gz., tgz, zip, jar等


### 编辑远程文件
```
vim scp://user@myserver[:port]//path/to/file.txt
```

### 纵向批量编辑
ctrl+v 进入纵向编辑模式
G 移动游标到最后一行，可视块覆盖所要修改的列
r 进入修改模式


## 带插件使用vim
{% post_link vim-plugin-note 点击这里查看带插件使用vim/

