# Vimium Cheatsheet

使用vim快捷键去操作chrome浏览器的插件

>?：快捷帮助

| 按键 | 功能 |
| ---- | ---------------- |
| hjkl | 网页四个方向拖动 |
| d/u | 上下翻半屏 |
| H/L | 页面后退/页面前进 |
| gg/G | 顶部/底部 |
| f/F | 选择链接打开/选择链接在新标签页打开 |
| r | 刷新页面 |
| yy | 复制url |
| p/PP | 打开剪贴板中url/新标签页打开剪切板url |
| gi | 激活第一个输入框 (2gi第二个输入框) |
| o/O | 打开url，书签，历史记录 / 新标签页打开url，书签，历史记录 |
| b/B | 打开书签 / 新标签打开书签1 |
| T | 搜索已经打开标签 |
| t | x新建页面 |
| J/K | 左右标签页 |
| alt + p | 固定当前页 |
| x | 关闭当前标签 |
| X | 恢复标签页 |
| yt | 复制标签页 |
