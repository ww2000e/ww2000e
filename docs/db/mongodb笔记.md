>https://blog.csdn.net/five3/article/details/54632932?utm_source=copy
### 2.6.x版本的设置
#### mongodb用户权限设置
``` shell
#vim /etc/mongodb.conf
去掉auth=true注释
重启服务
```

设置root,用户密码
```
>use admin
>db.createUser({user:'root',pwd:'root',roles:[{role:"dbAdminAnyDatabase",db:"admin"},{role:"userAdminAnyDatabase",db:"admin"}]})
退出后使用root账户登录
#mongo -u root -p root
>use youdb
>db.createUser({user:'user',pwd:'user',roles:[{role:"read",db:"youdb"},{role:"readWrite",db:"youdb"}]})  
设置成功后退出登录测试
#mongo -u user -p user --authenticationDatabase youdb
```


### 3.4.x版本的设置
待补充


### 4.0.x版本的设置
* 不使用认证权限启动

* 创建用户管理员
```
>use admin
>db.createUser(
  {
    user: "myUserAdmin",
    pwd: "abc123",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" }, "readWriteAnyDatabase" ]
  }
)
```

* 开启认证权限，用--auth参数启动，或者配置文件开启
```
security:
  authorization: enabled
```

* 用刚刚配置的用户管理员登陆
```
mongo --port 27017 -u "myUserAdmin" -p "abc123" --authenticationDatabase "admin"
```

* 创建用户
```
use test
db.createUser(
  {
    user: "myTester",
    pwd: "xyz123",
    roles: [ { role: "readWrite", db: "test" },
             { role: "read", db: "reporting" } ]
  }
)
```



### 导出csv
>http://yangcongchufang.com/mongo-export-csv.html
```
# 直接从某个表导出期望字段，生成CSV
mongoexport --host 10.8.8.111 --db sampleData --collection eventV4 --csv --out events.csv --fields '_id,something'

# 增加一个检索filter后导出CSV
mongoexport --host 10.8.8.111 --db sampleData --collection eventV4 --queryFile ./range.json --csv --out events.csv --fields '_id,something'
```
