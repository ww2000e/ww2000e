# git笔记

> 参考教程：沉浸式学Git http://igit.linuxtoy.org/contents.html


## 设置编辑器
``` shell
git config --global core.editor "vim"
```

## clone指定分支
``` shell
git clone -b 分支名  http://xxxxxxx
```

## alisa别名
``` shell
$vim ~/.gitconfig

[alisa]
  co = checkout
  ci = commit
  st = status
  br = branch
  hist = log --pretty=format:'%h %ad | %s%d [%an]' --graph --date=short
  type = cat-file -t
  dump = cat-file -p
```


## 保存密码
>- 设置记住密码（默认15分钟）：
>  git config --global credential.helper cache
>
>- 如果想自己设置时间，可以这样做：
>git config credential.helper 'cache --timeout=3600'
>这样就设置一个小时之后失效
>
>- 长期存储密码：
>git config \-\-global credential.helper store
>
>- 增加远程地址的时候带上密码也是可以的。(推荐)
>http://yourname:password@git.oschina.net/name/project.git

## 退回本地未提交更改
删除untracked的文件
``` shell
$git clean -df
```
将tracked文件退回，不写hash则退回上一个版本
``` shell
$git reset --hard <hash>
```

## 删除文件保留本地文件
``` shell
$git rm -r --cached some-directory
```

## tag
``` shell
$ git tag -a 'v1.5' -m '注释'    		//创建本地标签
$ git push origin v1.5          		//提交本地标签到远程
$ git push origin --tags        		//提交所有本地标签到远程
$ git tag -l                    		//标签列表
$ git tag -d v1.5               		//删除本地标签
$ git push origin --delete tag v1.5     //删除远程标签
```

## 下载子模块
``` shell
git submodule update --init --recursive
```

## 删除子模块
``` shell
git submodule deinit 模块名字

# 删除.gitmodules中记录的模块信息（--cached选项清除.git/modules中的缓存）
git rm --cached 模块名字

# 提交更改到代码库，可观察到'.gitmodules'内容发生变更
git commit -am "Remove a submodule." 
```

## submodule用法
``` shell
# 拉取子模块
git submodule add https://github.com/cofess/hexo-theme-pure themes/pure
```

## fork后合并原作者更新
``` shell
git remote add upstream https://github.com/原作者地址

echo "获取远程源的更新。"
git fetch upstream

echo "合并到本地库中。"
git merge upstream/master

echo "提交到本地版本库中。"
git commit -a -m "merged upstream."

echo "推送提交到自己的github库中。"
git push

```

