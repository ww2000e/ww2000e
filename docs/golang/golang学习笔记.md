记录学习golang的笔记



# 数组

* 数组存储的类型可以是内置类型，如整型，字符串，或者是某种结构类型。

* 指定长度声明，声明后不可变，**go语言声明变量总会使用零值初始化，数组也是**

  ```go
  var array [5]int
  ```

* 其他初始化方式

  ```
  // 指定数量和初始值
  array := [5]int{1,2,3,4,5}
  
  // 用...自动得到数组长度
  array := [...]int{1,2,3,4,5}
  
  // 指定下标位置，初始化指定位置数组
  array := [5]int{1:10, 2:20}
  ```
  
* 使用数组

  ```
  // 直接访问, 给下标为2的位置替换
  array[2] = 35
  ```

* 每个元素都是指针的数组,  使用*访问

  ```
  array := [5]*int{0: new(int), 1: new(int)}
  *array[0] = 10
  ```

* 数组直接用=即可复制，**复制后，两个数组值完全一样**, 只有**数组长度和每个元素类型**相同才能互相赋值

* 指针数组赋值给指针数组后，两个数组指向同一个字符串空间

  ```
  var array1 [3]*string
  array2 := [3]*string{new(string), new(string),new(string)}
  *arrary2[0]="red"
  *array2[1]="blue"
  *array2[2]="green"
  array1 = array2
  ```

* 多维数组

  ```
  var array [4][2]int
  array := [4][2]int{{10,11}, {20,21}, {30,31}, {40,41}}
  array := [4][2]int{1: {20,21}, 3:{40,41}} //指定索引位置初始化
  array := [4][2]int{1: {0:20}, 3: {1: 41}} //指定索引[1][0],[3][1]位置初始化
  ```

* 占用较大内存的变量可以**使用指针传递函数参数**

# 切片

* 切片是一种**数据结构**

* 切片有3个字段，分别是**指向底层数组的指针**，**长度**，**容量(最大长度)**

* 创建和初始化

  ```
  // make创建
  slice := make([]string, 5) //长度 容量 都是5
  slice := make([]int, 3, 5) //长度3 容量5
  // 字面量创建
  slice := []string{"a","b","c"} //长度 容量 都是3
  ```

* [ ]里面**不指定值**的时候才是**切片**，否则是数组

  ```
  slice := []string{99: ""} 
  ```

* nil切片创建方法

  ```
  var slice []int
  slice := make([]int, 0)
  slice := []int{}
  ```

* 切片的切片

  ```
  newSlice := slice[2:4] //取slice索引2到索引4-1内数据，长度等于4-2，容量为slice容量减2
  ```

* 切片append，容量不够会新建一个底层数组，当前值复制到新数组中，再追加新的值

* 第三个索引可以限制容量

  ```
  newSlice := slice[2:3:4] //从索引2开始到索引4-1地方为容量
  ```

* append追加切片

  ```
  append(s1,s2...) // ...运算符，可变参数，将s2所有元素追加到s1里面
  ```

* for遍历切片

  ```
  //配合range遍历切片
  for index, value := range slice {
  	fmt.Printf("index: %d  value:%d\n", index, value)
  }
  //传统方式
  for index := 2; index < len(slice); index ++ {
  	fmt.Printf("index: %d  value:%d\n", index, slice[index])
  }
  ```

* _(下划线)可以忽略索引值

* 多维切片

  ```
  slice := [][]int{{10}, {100,200}}
  slice[0] = append(slice[0], 20) //追加一个元素
  ```

* 切片在64位机器上占用24字节内存，不包含底层数组，尺寸很小，**函数传递切片成本很低**

# 映射

* 映射是一种数据结构，存储一系列无序键值对

* 创建，初始化，使用

  ````
  dict := make(map[string]int)
  dict := map[string]string{"aaaa": "1111", "bbbb": "2222"}
  dict["cccc"] = "3333"
  ````

* 切片，函数，包含切片的结构类型不能作为映射的键

* nil映射不能赋值

  ```
  var colors map[string]string //不可以赋值
  ```

* 判断键是否存在

  ```
  //方法一
  value, exists := colors["blue"]
  if exists {
  	fmt.Println(value)
  }
  //方法二
  value := colors["blue"]
  if value != "" {
  	fmt.Println(vlaue)
  }
  ```

* range迭代映射

  ```
  for key, value := range colors {
  	fmt.Println("k: %s  v:%s", key, value)
  }
  ```

* 删除一项

  ```
  delete(colors, "red")
  ```

* 函数间传递映射**不会**制造出映射的副本

#  类型系统

* 内置类型，语言本身提供的，分别是数值类型，字符串类型和布尔类型

* 引用类型，分别是：切片，映射，通道，接口和函数类型。创建的变量称作标头值。标头值是一个指向底层数据结构的指针。每个引用类型包含一组独特字段，管理底层数据结构。标头值里面包含指针，通过复制来传递引用类型的副本，本质上就是共享底层数据结构。

* 结构类型。用来描述一组数据值

* 用户定义类型。

  ```
  // 定义
  type user struct {
  	name string
  	ext int
  	privileged bool
  }
  // 声明变量
  var bill user
  // 声明时初始化所有字段
   bill := user{
  	name: "aaaaaa",
  	ext: 1,
  	privileged: true,
  }
  // 不用字段名初始化结构体
  bill := user{"aaaaa", 1, true}
  // 嵌套结构体
  type admin struct {
  	person user
  	level string
  }
  // 声明并初始化结构体
  fred := admin{
  	person: user{
  		name: "aaaaaa",
  		ext: 1,
  		privileged: true,
  	}
  	level: "super",
  }
  ```

* 编译器**不会**对不同类型值做隐式转换

* 类型可以添加方法，也就是函数

  ```
  // 在func和方法名之间增加一个参数
  type user struct {
  	name string
  	email string
  }
  
  func (u user) notify() {
  	fmt.Printf("name:%s  email: %s\n", u.name, u.email)
  }
  ```

* 多态。指代码可以根据类型的具体实现采取不同行为的能力。

* 接口是用来定义行为的类型。这些被定义的行为不由接口直接实现。而是通过方法由用户定义的类型实现。

* 用户定义的类型叫做实体类型。

* 方法集定义了接口的接受规则。

* 方法集规范

  | values | methods receivers |
  | ------ | ----------------- |
  | T      | (t T)             |
  | *T     | (t T) and (t *T)  |

* 接收者角度

  | methods receivers | values   |
  | ----------------- | -------- |
  | (t T)             | T and *T |
  | (t *T)            | *T       |

  
  
* 多态例子

  ```
  package main
  
  import (
	"fmt"
  )
  
  type notifier interface {
  	notify()
  }
  
  type user struct {
  	name string
  	email string
  }
  
  func (u *user) notify() {
  	fmt.Printf("*user notify name:%s email:%s\n",  u.name, u.email)
  }
  
  type admin struct {
  	name string
  	email string
  	admin string
  }
  
  func (a *admin) notify() {
  	fmt.Printf("*admin notify name:%s email:%s admin: %s\n",  a.name, a.email, a.admin)
  }
  
  func main() {
  	u := user{"bill", "bill@t.com"}
  	sendNotification(&u)
  
  	ad := admin{"lisa", "lisa@t.com", "admin"}
  	sendNotification(&ad)
  }
  
  func sendNotification(n notifier) {
  	n.notify()
  }
  ```

* 嵌入类型。将已有类型直接声明在新的结构类型里面

  ```
  type user struct {
  	name string
  	email string
  }
  
  type admin struct {
  	user
  	level string
  }
  
  func (u *user) notify() {
  	fmt.Printf("")
  }
  ```

* 初始化

  ```
  ad := admin{
  	user: user {
  		name: "john",
  		email: "john@t.com"
  	},
  	level: "super",
  }
  ```

* 方法访问

  ```
  //直接访问内部类型方法
  ad.user.notify()
  //内部类型方法也被提升到外部类型
  ad.notify()
  ```

* 接口参数也可以内部提升到外部，还可以用外部类型实现接口

  ```
  package main
  
  import (
          "fmt"
  )
  
  type ni interface {
          notify()
  }
  
  type user struct {
          name string
          email string
  }
  
  func (u *user) notify() {
          fmt.Printf("n:%s  e:%s\n", u.name, u.email)
  }
  
  type admin struct {
          user
          level string
  }
  
  func (a *admin) notify() {
          fmt.Printf("admin n:%s e:%s\n", a.name, a.email)
  }
  
  func main() {
          ad := admin{
                  user: user{
                          name: "john",
                          email: "john@t.com",
                  },
                  level: "super",
          }
  
          sendN(&ad)
  
          ad.user.notify()
  
          ad.notify()
  }
  
  func sendN(n ni) {
          n.notify()
  }
  ```

  

* 通过大小写区分公开或未公开的标识符

# 并行

* **并行**是让不同的代码片段同时在不同的物理处理器上执行，并行的关键是同时做很多事情。**并发**是指同时管理很多事情，这些事情可能只做了一半就去做别的事情

* goroutine分配**多个**逻辑处理器才可以并行

* go build -race 可以检测是否有竞争状态

* 竞争状态传统解决方法，加锁

  ```
  // 原子函数
  // 原子函数能够以很底层的加锁机制来同步访问整形变量和指针
  import (
  	"sync/atomic"
  )
  
  ...
  	atomic.AddInt64(&counter, 1)  整形加
  	LoadInt64()  读整形
  	StoreInt64() 写整形
  ...
  
  //互斥锁
  import (
  	"sync"
  )
  '''
  	var (
  		mutex sync.Mutex
  	)
  	
  	mutex.Lock()
  	''''
  	mutex.Unlock()
  '''
  ```

* 通道。 当一个资源需要在goroutine之间共享时，通道在goroutine之间架起了一个管道，并提供了确保同步交换数据的机制

* 声明通道。需要指定共享的数据类型。支持 内置类型，命名类型，结构类型，引用类型，指针

* 内置函数make创建通道

  ```
  // 无缓冲通道
  unbuffered := make(chan int)
  
  // 有缓冲的字符串通道
  buffered := make(chan string, 10)
  //发送字符串至管道
buffered <- "gopher"
  //通道接收字符串
  value := <-buffered
  ```
  
  

