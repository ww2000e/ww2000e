记录各个发行版桌面日常使用问题

## gnome3给home目录的默认文件夹切换成英文
>参考：http://dbua.iteye.com/blog/943945

home目录下中文在shell里面用起来很麻烦，网上找了修改方法
``` shell
$export LANG=en_US
$xdg-user-dirs-gtk-update
$export LANG=zh_CN.UTF-8
```
