# shell使用笔记
## linux的shell里面后台运行程序总会提示进程号和进程完成的提示
类似这样：

``` shell
$ls &
[1] 119788
$ 
[1] + 119788 done
```

如果不需要提示，可以这样解决：

``` shell
$(ls &)
```

加一对括号即可,让它在子shell里面执行


## 命令行界面的天气预报
``` shell
curl http://wttr.in/
```

## asciiquarium
字符界面的水族馆

## 常用命令

### 查看正在使用什么shell
``` shell
ps -p $$
echo $0
```

### 查看所有已安装shell
``` shell
cat /etc/shells
```

### grep
``` shell
grep forest f.txt     #文件查找
grep forest f.txt cpf.txt #多文件查找
grep 'log' /home/admin -r -n #目录下查找所有符合关键字的文件
cat f.txt | grep -i shopbase    
grep 'shopbase' /home/admin -r -n --include *.{vm,java} #指定文件后缀
grep 'shopbase' /home/admin -r -n --exclude *.{vm,java} #反匹配
seq 10 | grep 5 -A 3    #上匹配
seq 10 | grep 5 -B 3    #下匹配
seq 10 | grep 5 -C 3    #上下匹配，平时用这个就妥了
cat f.txt | grep -c 'SHOPBASE'
```

### awk
* 基础
``` shell
awk '{print $4,$6}' f.txt
awk '{print NR,$0}' f.txt cpf.txt    
awk '{print FNR,$0}' f.txt cpf.txt
awk '{print FNR,FILENAME,$0}' f.txt cpf.txt
awk '{print FILENAME,"NR="NR,"FNR="FNR,"$"NF"="$NF}' f.txt cpf.txt
echo 1:2:3:4 | awk -F: '{print $1,$2,$3,$4}'
awk '{S[$1]++}END{for(k in S) print k,S[k]}' access-log.txt
awk '/^tcp/ {S[$NF]++}END{for(k in S) print S[k],k}' ss.txt | sort -rn | head
```

* 匹配
``` shell
awk '/ldb/ {print}' f.txt   #匹配ldb
awk '!/ldb/ {print}' f.txt  #不匹配ldb
awk '/ldb/ && /LISTEN/ {print}' f.txt   #匹配ldb和LISTEN
awk '$5 ~ /ldb/ {print}' f.txt #第五列匹配ldb
```

* 内建变量
NR:NR表示从awk开始执行后，按照记录分隔符读取的数据次数，默认的记录分隔符为换行符，因此默认的就是读取的数据行数，NR可以理解为Number of Record的缩写。

FNR:在awk处理多个输入文件的时候，在处理完第一个文件后，NR并不会从1开始，而是继续累加，因此就出现了FNR，每当处理一个新文件的时候，FNR就从1开始计数，FNR可以理解为File Number of Record。

NF: NF表示目前的记录被分割的字段的数目，NF可以理解为Number of Field

### sort, uniq
``` shell
去除相邻重复
uniq file.txt 

排序去重
sort -u file.txt

倒序排列
sort -r file.txt 

统计重复个数
sort file.txt | uniq -c

指定列排序
sort -k1 file.txt
```

### tr
待补充

### tee
待补充

### find
``` shell
sudo -u admin find /home/admin /tmp /usr -name \*.log(多个目录去找)
find . -iname \*.txt(大小写都匹配)
find . -type d 目录
             f 普通文件
             c 字符设备
             b 块设备
             l 符号连结
             p 管道
             s unix套接字文件  
find . -size [-|+] 小于/大于  c 行数  k/M/G 字节大小 -empty 空文件

find /usr -type l(当前目录下所有的符号链接)
find /usr -type l -name "z*" -ls(符号链接的详细信息 eg:inode,目录)
find /home/admin -size +250000k(超过250000k的文件，当然+改成-就是小于了)
find /home/admin f -perm 777 -exec ls -l {} \; (按照权限查询文件)
find /home/admin -atime -1  1天内访问过的文件
                        1   1天前访问过
                        +1  1天没访问过
find /home/admin -ctime -1  1天内状态改变过的文件    
find /home/admin -mtime -1  1天内修改过的文件
find /home/admin -amin -1  1分钟内访问过的文件
find /home/admin -cmin -1  1分钟内状态改变过的文件    
find /home/admin -mmin -1  1分钟内修改过的文件
find . -perm /664   任意一位匹配
             -664   完全匹配
       -exec  找到文件后执行命令
find /tmp -user root    查找用户名root的文件
find /tmp -group  root  查找组为root的文件
          -uid          按uid查找
          -gid          按gid查找
find /tmp -nouser      查找没有用户的文件
```

## 其他问题记录
###  kill掉bash中while循环
``` shell
// 当写了 while [[ 1 ]]; do somecmd; sleep 60; done &后如何kill掉此循环
// 执行命令找出关系
$ps fjx
2226  9411  9411  8383 ?           -1 S     1000   0:00  \_ /bin/bash
9411 17674  9411  8383 ?           -1 Sl    1000   0:00      \_ somecmd
// 9411就是执行while进程
```



### centos的yum报错$releasever

别人装好的centos一个源都没有，配置源后又和redhat-release冲突执行，给redhat-release删了后，执行yum命令的时候提示
http://mirrors.aliyun.com/centos/%24releasever/addons/x86_64/repodata/repomd.xml
应该是环境变量$releasever没了,yum --releasever=7 install centos-release后依然无效。不知道这个变量存在哪，网上找了个设置方法。。

解决方法:
``` shell
$echo 7 > /etc/yum/vars/releasever
```


### 其他
* 查看系统启动耗时
``` shell
# 启动耗时汇总
systemd-analyze
# 查看服务明细启动耗时
systemd-analyze blame
```
* 查看正在使用什么shell
``` shell
ps -p $$
echo $0
```
* 查看所有已安装shell
``` shell
cat /etc/shells
```
* 修改shell
``` shell
chsh -s /bin/ksh
```

> 参考： 
> https://yq.aliyun.com/articles/69520?utm_content=m_10360

