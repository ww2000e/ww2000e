网上搜索了一番，找到一个bash补全和ssh_config配合，管理ssh的方法

## ssh_config配置
1. 配置文件默认地址 ~/.ssh/config

2. 配置示例
```
Host localhost
    HostName 127.0.0.1
    User root
```

## bash-completion
1. 安装bash-completion

2. 配置
创建/etc/bash_completion.d/ssh写入
``` bash
_ssh() 
{
    local cur prev opts
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    opts=$(grep '^Host' ~/.ssh/config ~/.ssh/config.d/* 2>/dev/null | grep -v '[?*]' | cut -d ' ' -f 2-)

    COMPREPLY=( $(compgen -W "$opts" -- ${cur}) )
    return 0
}
complete -F _ssh ssh
```


完成以上配置后，直接在命令输入ssh空格，敲tab就能看到ssh_config中配置的项目
