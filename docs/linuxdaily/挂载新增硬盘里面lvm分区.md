电脑上的vmware换成virtualbox后，想给旧的虚机硬盘挂载到虚机里面，发现lvm分区不能直接挂载，以下是挂载的过程。


## 用到的命令

* vgscan

  ```
  查找存在的lvm卷组
  ```

  

* vgdisplay

  ```
  显示lvm卷组信息
  ```

  

* vgrename

  ```
  重命名卷组的名称
  ```

  

* vgchange

  ```
  修改卷组的属性，经常是用来修改卷组是否处于活跃状态
  ```

* lvsan

  ```
  系统中的所有逻辑卷
  ```

  

### 解决过程

vgdisplay查看两个硬盘是同名的，所以修改新挂载的硬盘名称

```
vgrename <vg uuid> <new name>
```

修改完后激活卷组

```
vgchange -ay /dev/<new name>
```

激活后用lvsan可以看到逻辑卷被激活了，状态active，最后使用mount挂载lvsan看到的分区即可



