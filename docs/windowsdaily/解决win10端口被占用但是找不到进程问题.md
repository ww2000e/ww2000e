最近win10开机，安装的服务用的端口总是显示被占用，比如1080端口，往后试了好几个都不行，用netstat查找进程又不存在，后来网上搜索资料发现，这是win10的  [KB4074588](https://support.microsoft.com/eu-es/help/4074588/windows-10-update-kb4074588)   补丁已知问题，系统预留了一些段，可以用命令查看系统保留了哪些端口段：

``` powershell
netsh interface ipv4 show excludedportrange protocol=tcp
```

解决方法就是避开这些段。。
